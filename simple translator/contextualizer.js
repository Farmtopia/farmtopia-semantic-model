const jsonldContext = {
  "fsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/FarmtopiaSemanticModel.rdf#",
  "pcsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/ontologies/ploutos.rdf#",
  "sosa": "http://www.w3.org/ns/sosa/",
  "ssn-ext": "http://www.w3.org/ns/ssn/ext/",
  "saref4agri": "https://saref.etsi.org/saref4agri/",
  "geosparql": "http://www.opengis.net/ont/geosparql/",
  "qudt": "http://qudt.org/schema/qudt/",
  "unit": "http://qudt.org/2.1/vocab/unit",
  "foodie": "http://foodie-cloud.com/model/foodie#",
  "skos": "http://www.w3.org/2004/02/skos/core#",
  "schema": "http://schema.org/",
  "cf": "http://purl.oclc.org/NET/ssnx/cf/cf-property#",
  "ids": "https://w3id.org/idsa/core#",
  "dct": "http://purl.org/dc/terms/",
  "odlr": "http://www.w3.org/ns/odrl/2/",
  "obo": "http://purl.obolibrary.org/obo/",
  "contains": {
    "@id": "fsm:contains",
    "@type": "@id",
    "@container": "@set"
  },
  "nutrientConcentration": {
    "@id": "pcsm:hasNutrientConcentration",
    "@type": "@id",
    "@container": "@set"
  },
  "operatedOn": {
    "@id": "pcsm:isOperatedOn",
    "@type": "@id"
  },
  "geometry": {
    "@id": "geosparql:hasGeometry",
    "@type": "@id"
  },
  "centroid": {
    "@id": "pcsm:centroid",
    "@type": "@id"
  },
  "resourceQuantityUsed": {
    "@id": "fsm:resourceQuantityUsed",
    "@type": "@id"
  },
  "relatedConcept": {
    "@id": "fsm:relatedConcept",
    "@type": "@id"
  },
  "fertilizer": {
    "@id": "pcsm:usesFertilizer",
    "@type": "@id"
  },
  "pesticide": {
    "@id": "pcsm:usesPesticide",
    "@type": "@id"
  },
  "name": "saref4agri:hasName",
  "cropVariety": "pcsm:hasCropVariety",
  "geoJson": {
    "@id": "geosparql:asGeoJSON",
    "@type": "@json"
  },
  "hasArea": {
    "@id": "fsm:hasArea",
    "@type": "@id"
  },
  "unitOfMeasure": {
    "@id": "fsm:unitOfMeasure",
    "@type": "@id"
  },
  "usedProcedure": {
    "@id": "sosa:usedProcedure",
    "@type": "@id"
  },
  "implementedBy": {
    "@id": "sosa:implementedBy",
    "@type": "@id"
  },
  "operationNotes": "fsm:operationNotes",
  "applicationMethod": "pcsm:hasApplicationMethod",
  "startDatetime": "pcsm:hasStartDatetime",
  "endDatetime": "pcsm:hasEndDatetime",
  "activeSubstance": "pcsm:hasActiveSubstance",
  "targetedTowards": "pcsm:isTargetedTowards",
  "numericValue": "qudt:numericValue",
  "vocabCode": "skos:notation",
  "isEcoFarm": "fsm:isEcoFarm",
  "contents": {
    "@id": "sosa:hasMember",
    "@type": "@id",
    "@container": "@set"
  },
  "property": {
    "@id": "sosa:property",
    "@type": "@id"
  },
  "layer": {
    "@id": "fsm:layer",
    "@type": "@id"
  },
  "sourceSensor": {
    "@id": "sosa:madeBySensor",
    "@type": "@id"
  },
  "location": {
    "@id": "fsm:location",
    "@type": "@id"
  },
  "connectors": {
    "@id": "ids:listedConnector",
    "@type": "@id",
    "@container": "@set"
  },
  "connectorCatalog": {
    "@id": "ids:connectorCatalog",
    "@type": "@id",
    "@container": "@set"
  },
  "representation": {
    "@id": "ids:maintainer",
    "@type": "@id"
  },
  "serviceResources": {
    "@id": "dcat:servesDataset",
    "@type": "@id",
    "@container": "@set"
  },
  "serviceType": {
    "@id": "dct:type",
    "@type": "@id",
    "@container": "@set"
  },
  "instance": {
    "@id": "ids:instance",
    "@type": "@id"
  },
  "sliceContents": {
    "@id": "fsm:sliceContents",
    "@type": "@id",
    "@container": "@set"
  },
  "datasetSlices": {
    "@id": "fsm:datasetSlices",
    "@type": "@id",
    "@container": "@set"
  },
  "creator": {
    "@id": "dct:creator",
    "@type": "@id"
  },
  "publisher": {
    "@id": "dct:publisher",
    "@type": "@id"
  },
  "curator": {
    "@id": "dct:curator",
    "@type": "@id"
  },
  "legalName": "schema:legalName",
  "address": "schema:address",
  "email": "schema:email",
  "representationStandard": "ids:representationStandard",
  "license": {
    "@id": "dct:license",
    "@type": "@id"
  },
  "rights": {
    "@id": "dct:rights",
    "@type": "@id"
  },
  "policy": {
    "@id": "odlr:hasPolicy",
    "@type": "@id"
  },
  "permission": {
    "@id": "odlr:permission",
    "@type": "@id"
  },
  "language": "dct:language",
  "title": "dct:title",
  "description": "dct:description",
  "comment": "fsm:comment",
  "label": "fsm:label",
  "toponym": "fsm:toponym",
  "dateTime": "sosa:resultTime",
  "value": "sosa:hasSimpleResult",
  "from": "fsm:from",
  "to": "fsm:to",
  "timestamp": "fsm:timestamp",
  "phenomenonTime": {
    "@id": "sosa:phenomenonTime"
  },
  "Atmosphere": {
    "@id": "cf:atmosphere"
  },
  "Vegetation": {
    "@id": "cf:vegetation"
  },
  "Water": {
    "@id": "cf:water"
  },
  "Surface": {
    "@id": "cf:surface"
  },
  "Biosphere": {
    "@id": "fsm:biosphere"
  },
  "NDVI": {
    "@id": "fsm:normalized_difference_vegetation_index"
  },
  "EVI": {
    "@id": "fsm:enhanced_vegetation_index"
  },
  "STR": {
    "@id": "fsm:shortwave_infrared_transform_reflectance"
  },
  "LST": {
    "@id": "fsm:land_surface_temperature"
  },
  "VHP": {
    "@id": "fsm:vertical_horizontal_polarization"
  },
  "NDWI": {
    "@id": "fsm:normalized_difference_water_index"
  },
  "AirTemperature": {
    "@id": "fsm:air_temperature"
  },
  "Celsius": {
    "@id": "unit:DEG_C"
  },
  "RelativeHumidity": {
    "@id": "fsm:relative_humidity"
  },
  "Percent": {
    "@id": "unit:PERCENT"
  },
  "Index": {
    "@id": "fsm:INDEX"
  },
  "WindSpeed": {
    "@id": "fsm:wind_speed"
  },
  "MeterPerSecond": {
    "@id": "unit:M-PER-SEC"
  },
  "KilometerPerHour": {
    "@id": "unit:KiloM-PER-HR"
  },
  "PestInfestationRisk": {
    "@id": "fsm:pest_infestation_risk"
  },
  "WindDirection": {
    "@id": "fsm:wind_direction"
  },
  "Degree": {
    "@id": "unit:DEG"
  },
  "Precipitation": {
    "@id": "fsm:presipitation"
  },
  "Evapotranspiration": {
    "@id": "fsm:evapotranspiration"
  },
  "Millimetre": {
    "@id": "unit:MilliM"
  },
  "AirPressure": {
    "@id": "fsm:air_pressure"
  },
  "LeafWetness": {
    "@id": "fsm:leaf_wetness"
  },
  "LeafTemperature": {
    "@id": "fsm:leaf_temperature"
  },
  "Hectopascal": {
    "@id": "unit:HectoPA"
  },
  "Millibar": {
    "@id": "unit:MilliBAR"
  },
  "Soil": {
    "@id": "cf:soil_layer"
  },
  "SolarIrradiance": {
    "@id": "fsm:solar_irradiance"
  },
  "SoilTemperature": {
    "@id": "fsm:soil_temperature"
  },
  "SoilMoisture": {
    "@id": "fsm:soil_moisture"
  },
  "SoilSalinity": {
    "@id": "fsm:soil_salinity"
  },
  "UltravioletIrradiance": {
    "@id": "fsm:ultraviolet_irradiance"
  },
  "WattPerMetre2": {
    "@id": "unit:MicroW-PER-M2"
  },
  "Volt": {
    "@id": "unit:V"
  },
  "Hectare": {
    "@id": "unit:HA"
  },
  "landType": {
    "@id": "fsm:landType"
  },
  "nationalIdentifier": "fsm:nationalIdentifier",
  "agrovocCode": "fsm:agrovocCode",
  "eppoCode": "fsm:eppoCode",
  "OBIClass": {
    "@id": "fsm:OBIClass",
    "@type": "@id"
  },
  "hasIrrigationSystem": {
    "@id": "fsm:hasIrrigationSystem",
    "@type": "@id"
  },
  "plantGrowthStage": "fsm:plantGrowthStage",
  "hasIrrigationType": "pcsm:hasIrrigationType",
  "plantsPerHa": "fsm:plantsPerHa",
  "hasPlantDate": "pcsm:hasPlantDate",
  "hasHarvestDate": "pcsm:hasHarvestDate",
  "aggregation": "fsm:aggregation",
  "metric": "fsm:metric",
  "ratio": "productRatio"
};

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized representation object of a task.
 *
 * @param {String} taskId - unique identifier for the task
 * @param {String} taskLabel - description of the task
 * @param {String} correlatedTaskLabel - FSM vocabulary term for the description of the task
 * @param {String} startDatetime - ISO8601 datetime refering to the start time of the task
 * @param {String} endDatetime - ISO8601 datetime refering to the end time of the task
 * @param {String} note - note
 * @param {String} operateOnFieldId - unique identifier of the field operated on by the task
 * @returns {Object} - contexualized farm object
 */
const mapTask = (taskId, taskLabel, correlatedTaskLabel, startDatetime, endDatetime, note, operateOnFieldId) => (
  {
    "@id": taskId,
    "@type": correlatedTaskLabel,
    label: taskLabel,
    startDatetime: startDatetime,
    endDatetime: endDatetime,
    note: note,
    operatedOn: {
      "@id": operateOnFieldId
    }
  }
);

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized representation object of a farm.
 *
 * @param {String} farmId - unique identifier for the farm
 * @param {Float} latitude - farm location latitude
 * @param {Float} longitude - farm location longitude
 * @param {Boolean} isEcoFarm - whether farm is eco
 * @returns {Object} - contexualized farm object
 */
const mapFarm = (farmId, latitude, longitude, isEcoFarm) => {
  const id = generateRandomId(1);
  return {
    "@id": farmId,
    "@type": "fsm:FarmComplex",
    centroid: {
      "@id": `_:geometry${id}Centroid`,
      "@type": "geosparql:Geometry",
      geoJson: {
        type: "Point",
        coordinates: [ latitude, longitude ]
      }
    },
    isEcoFarm: isEcoFarm
  }
};

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized representation object of a Crop.
 *
 * @param {String} id - unique identifier for the crop
 * @param {String} cropTypeName - crop type (e.g. Wheat (winter))
 * @param {String} cropTypeVocIdentifier - crop type Voc identifier
 * @param {Boolean}  hasPlantDate - Date of planting
 * @param {Boolean}  hasHarvestDate - Date of harvest
 * @returns {Object} - contexualized crop object
 */
const mapCrop = (id, cropTypeName, cropTypeVocIdentifier, hasPlantDate, hasHarvestDate) => (
  {
    "@id": id,
    "@type": "pcsm:Crop",
    cropVariety: cropTypeName,
    agrovocCode: cropTypeVocIdentifier,
    hasPlantDate: hasPlantDate,   
    hasHarvestDate: hasHarvestDate  
  }
);

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized representation object of a FarmSite with a Plot.
 *
 * @param {String} id - unique identifier for the crop
 * @param {String} nationalIdentifier - national identification number of the field
 * @param {String} name - name of the field
 * @param {Float} area - field area in square meters  
 * @param {Object} geoJSON - geoJSON object representing field's spatial features
 * @param {String} landTypeName - land type Voc identifier
 * @param {String} landTypeVocIdentifier - field type Voc term
 * @param {Array} cropObjects - array of FSM crop objects
 * @returns {Object} - contexualized FarmSite object
 */
const mapField = (id, nationalIdentifier, name, area, geoJSON, landTypeName, landTypeVocIdentifier, cropObjects) => [
  {
    "@id": id,
    "@type": "fsm:FarmSite",
    name: name,
    nationalIdentifier: nationalIdentifier,
    hasArea: {
      "@id": `${name}FieldArea`,
      "@type": "qudt:QuantityValue",
      numericValue: parseFloat(area * 0.0001).toFixed(8),
      unitOfMeasure: "unit:HA"
    },
    geometry: {
      "@id": `_:${name}Fieldgeometry`,
      "@type": "geosparql:Geometry",
      geoJSON: geoJSON
    },
    contains: [
      {
        "@id": `${name}FieldPlot`,
        "@type": "fsm:Plot",
        landType: {
          "@id": landTypeName.replace(/\s/g, ""),
          "@type": landTypeVocIdentifier,
          label: landTypeName
        },
        contains: cropObjects
      }
    ]
  }
];

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized location object
 *
 * @param {string} locationId - Unique identifier for the location
 * @param {string} locationName - Name of the location
 * @param {string} locationToponym - Toponym (place name) of the location
 * @param {number} longitude - Longitude coordinate of the location
 * @param {number} latitude - Latitude coordinate of the location
 * @returns {Object} - contextualized location object
 */
const mapLocation = (locationId, locationName, locationToponym, longitude, latitude) => {
  const id = generateRandomId(1);
  return [
    {
      id: locationId,
      "@type": "fsm:PointOfInterest",
      label: locationName,
      toponym: locationToponym,
      geometry: {
        "@id": `geometry${id}`, // DONE this should be a random/dynamic id
        "@type": "geometry",
        geoJson: {
          type: "Point",
          coordinates: [ longitude, latitude ] 
        }
      }
    }
  ]
}

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized measurement object as an observation
 *
 * @param {String} propertyName - the physical property measured
 * @param {String} sensorId - unique identifier of the sensor
 * @param {String} dateTime - ISO8601 datetime - when the measurement occured
 * @param {Float} value - value of the measurement
 * @returns {Object} - contextualized measurement object
 */
const mapMeasurement = (propertyName, sensorId, dateTime, value) => (
  {
    "@id": `${propertyName}Observation_${sensorId}`,
    "@type": "fsm:Observation",
    dateTime: dateTime,
    value: value
  }
);

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized data points group object for specific 
 * common property, source sensor and unit of measure, between contents of different measurements
 *
 * @param {String} propertyName - the physical property measured
 * @param {String} sensorId - unique identifier of the sensor
 * @param {String} unitOfMeasure - standard for measurement of the measured quantity
 * @param {Array} measurements - mapped measurement objects that correlate to property and sensor
 * @returns {Object} - contextualized observation collection object
 */
const mapObservationCollection = (propertyName, sensorId, unitOfMeasure, measurements) => {
  let mappedObservationCollection = {
    "@id": `${propertyName}ObservationCollection_${sensorId}`,
    "@type": "fsm:DataPointsGroup",
    property: propertyName,
    sourceSensor: {
      "@id": sensorId,
      "@type": "sosa:Sensor"
    },
    contents: measurements
  }
  if ( unitOfMeasure !== undefined ) {
    mappedObservationCollection.unitOfMeasure = unitOfMeasure;
  }
  return mappedObservationCollection;
};

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized data points group object for a specific 
 * common layer of reference between slice contents of different measurement groups
 *
 * @param {String} layer - name of the measurement layer
 * @param {Array} observationCollections - mapped observation collection that correlate to layer
 * @returns {Object} - contextualized measurement layer object
 */
const mapMeasurementLayerGroup = (layer, observationCollections) => (
  {
    "@id": `${layer}ObservationCollection`,
    "@type": "fsm:DataPointsGroup",
    layer: layer,
    sliceContents: observationCollections
  }
);

/**
 * Mapping raw data to FSM components function. 
 * Handles building a contextualized data points group object for a specific 
 * common location between slice contents of different measurement layers
 *
 * @param {String} sourceName - name of the source (eg Weather Station)
 * @param {String} locationId - unique identifier of the location object that correlates to measurements produced by source
 * @param {Array} measurementLayers - mapped measurement layers that correlate to source
 * @returns {Object} - contextualized measurement data points group object
 */
const mapMeasurementLocationGroup = (sourceName, locationId, measurementLayers) => {
  const id = generateRandomId(2)
  return {
    "@id": `${sourceName}_${id}Observations`,
    "@type": "fsm:DataPointsGroup",
    location: {
      "@id": locationId
    },
    sliceContents: measurementLayers
  }
};

/**
 * Contextualizing function: builds component of FSM data object
 * Handles building the final contexualized FSM dataset object
 *
 * @param {String} nameofDataset - name of the dataset (e.g. fields, tasks)
 * @param {Array} FSMDataObjects - array of FSM data objects, grouped by type
 * @param {Object} FSMDataObjects[].typeOfData - type of data described by FSM data Object (for naming)
 * @param {Object} FSMDataObjects[].dataObject - FSM data object(s)
 * @returns {Object} - FSM dataset object
 */
function contextualizeDataset(nameofDataset, FSMDataObjects) {
  const graph = {
    "@id": nameofDataset,
    "@type": "fsm:Dataset",
    datasetSlices: []
  };
  FSMDataObjects.forEach( ({typeOfData, FSMData}) => { 
    graph.datasetSlices.push( 
      {
        "@id": `${typeOfData}Slice`,
        "@type": "fsm:Slice",
        sliceContents: FSMData
      }
    )
  });
  return {
    "@context": jsonldContext,
    "@graph": [graph]
  };
}

/**
 * Util function for generating random numerical id of varying size to use them as unique identifiers
 * 
 * @param {Integer} length - number of id digits 
 * @returns {Integer} - numerical id
 */
function generateRandomId(length) {
  if (length < 1 || !Number.isInteger(length)) {
    throw new Error("Length must be a positive integer");
  }
  const min = 10 ** (length - 1);
  const max = (10 ** length) - 1;
  return Math.floor(min + Math.random() * (max - min + 1));
}

/**
 * Converts a Unix timestamp to an ISO 8601 formatted date string with a specified UTC offset.
 *
 * @param {Integer} unixTimestamp - The Unix timestamp to convert (in seconds).
 * @param {Integer} [offsetHours=3] - The UTC offset in hours.
 * @returns {String} The ISO 8601 formatted date string with the specified offset.
 */
function unixToISO8601(unixTimestamp, offsetHours = 3) {
  const date = new Date(unixTimestamp * 1000);
  const adjustedDate = new Date(date.getTime() + offsetHours * 60 * 60 * 1000);
  const year = adjustedDate.getUTCFullYear();
  const month = String(adjustedDate.getUTCMonth() + 1).padStart(2, '0');
  const day = String(adjustedDate.getUTCDate()).padStart(2, '0');
  const hours = String(adjustedDate.getUTCHours()).padStart(2, '0');
  const minutes = String(adjustedDate.getUTCMinutes()).padStart(2, '0');
  const seconds = String(adjustedDate.getUTCSeconds()).padStart(2, '0');
  const offsetSign = offsetHours >= 0 ? '+' : '-';
  const absoluteOffset = Math.abs(offsetHours);
  const offsetFormatted = `${offsetSign}${String(absoluteOffset).padStart(2, '0')}:00`;
  return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}${offsetFormatted}`;
}

module.exports = {
  mapTask,
  mapFarm,
  mapCrop,
  mapField,
  mapLocation,
  mapMeasurement,
  mapObservationCollection,
  mapMeasurementLayerGroup,
  mapMeasurementLocationGroup,
  contextualizeDataset,
  unixToISO8601
}

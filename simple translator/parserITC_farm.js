const fs = require("fs");
const contextualizer = require("./contextualizer")

// Read the input JSON file
const jsonFile = process.argv[2];
const jsonData = fs.readFileSync(jsonFile, 'utf-8');

// Parse the input JSON data
const jsonObject = JSON.parse(jsonData);

/**
* Parse raw field-related input data to fill required properties of FSM objects
* (key names should be same as in the input JSON file, values are the parsed values)
*/
let {
  uuid: farmId,
  latitude: latitude,
  longitude: longitude,
  ecoFarm: isEcoFarm,
} = jsonObject;

// Pass the corresponding parsed data as arguments to the mapFarm function
// to create farm FSM object in JSON-LD format
const farmObject = contextualizer.mapFarm(farmId, latitude, longitude, isEcoFarm);

// We group the FSM Objects by type of data and store in an array
const FSMDataObjects = [
  { 
    typeOfData: "FarmComplexes",
    FSMData: farmObject 
  }
];

// Pass the name of tha dataset and the FSMDataObjects to the contextualizer to create the final FSM dataset
const FSMData = contextualizer.contextualizeDataset("ITC_Farms", FSMDataObjects);

// Output JSON file path
const outputJSONFilePath = "./json/ITC2FSM_farm_output.json";

// write to JSON file
try { 
  fs.writeFileSync(outputJSONFilePath, JSON.stringify(FSMData, null, 2), 'utf8');
  console.log(`FSM data written to file ${outputJSONFilePath}`);
} catch (error) {
  console.error("Error writing file");
}

const fs = require("fs");
const contextualizer = require("./contextualizer")

// Read the input JSON file
const jsonFile = process.argv[2];
const jsonData = fs.readFileSync(jsonFile, 'utf-8');

// Parse the input JSON data
const jsonObject = JSON.parse(jsonData);

/**
* Parse raw field-related input data to fill required properties of FSM objects
* (key names should be same as in the input JSON file, values are the parsed values)
*/
let {
  uuid: taskId,
  taskType: { nameEn: taskLabel },
  dateFrom: startDatetime,
  dateTo: endDatetime,
  note: note,
  fields: operatedOnFieldsArray
} = jsonObject;

// FSM uses vocabularies to map some taxonomies terms.
// For correlating input taxonomy values to vocabulary terms (e.g. OBO, Agrovoc)
// we use a taxonomyCorrelation object (key names are the input taxonomy values, values are 
// the official vocabulary identifiers)
const taxonomyCorrelation = {
  "Removal of invasive plants": "pcsm:CuttingOperation" 
}

// Pass the corresponding parsed data as arguments on the mapTask function
// to create farm FSM object in JSON-LD format
const taskObjects = operatedOnFieldsArray.map( field => 
  contextualizer.mapTask(taskId, 
    taskLabel, 
    taxonomyCorrelation[taskLabel],
    startDatetime, 
    endDatetime, 
    note,
    field.fieldUuid
  )
);

// We group the FSM Objects by type of data and store in an array
const FSMDataObjects = [
  { 
    typeOfData: "Operation",
    FSMData: taskObjects 
  }
];

// Pass the name of tha dataset and the FSMDataObjects to the contextualizer to create the final FSM dataset
const FSMData = contextualizer.contextualizeDataset("ITC_Tasks", FSMDataObjects);

// Output JSON file path
const outputJSONFilePath = "./json/ITC2FSM_task_output.json";

// write to JSON file
try { 
  fs.writeFileSync(outputJSONFilePath, JSON.stringify(FSMData, null, 2), 'utf8');
  console.log(`FSM data written to file ${outputJSONFilePath}`);
} catch (error) {
  console.error("Error writing file");
}

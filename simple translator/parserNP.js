const fs = require("fs");
const contextualizer = require("./contextualizer")

// Read the input JSON file
const jsonFile = process.argv[2];
const jsonData = fs.readFileSync(jsonFile, 'utf-8');

// Parse the input JSON data
const jsonObject = JSON.parse(jsonData);

/**
* Parse raw field-related input data to fill required properties of FSM objects
* (key names should be same as in the input JSON file, values are the parsed values variables)
*/
let {
  location: {
    id: locationId,
    long: longitude,
    lat: latitude,
    toponym: locationToponym,
    name: locationName,
  },
  measurements: measurementsArray
} = jsonObject;

// Pass the parsed location data as arguments to the mapLocation
// to create location FSM objects in JSON-LD format
const locationObject = contextualizer.mapLocation(locationId, locationName, locationToponym, longitude, latitude);

// user-defined configuration for correlating sensor IDs to layers, properties and measurement units
const sensorConfiguration = {
  nplwrh1:              { propertyName: "LeafWetness", layer: "Vegetation"},
  nplwrh2:              { propertyName: "RelativeHumidity", unitOfMeasure: "Percent", layer: "Vegetation" },
  wsht30_rh:            { propertyName: "RelativeHumidity", unitOfMeasure: "Percent", layer: "Atmosphere" },
  dnd_90_sdi12_sm_10:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  dnd_90_sdi12_sm_20:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  dnd_90_sdi12_sm_30:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  dnd_90_sdi12_sm_40:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  dnd_90_sdi12_sm_50:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  dnd_90_sdi12_sm_60:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  dnd_90_sdi12_sm_70:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  dnd_90_sdi12_sm_80:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  dnd_90_sdi12_sm_90:   { propertyName: "SoilMoisture", unitOfMeasure: "Percent", layer: "Soil" },
  uv:                   { propertyName: "UltravioletIrradiance", unitOfMeasure: "Volt", layer: "Surface" },
  pyranometer:          { propertyName: "SolarIrradiance", unitOfMeasure: "Volt", layer: "Surface" },
  pressure:             { propertyName: "AirPressure", unitOfMeasure: "Millibar", layer: "Atmosphere" },
  dnd_90_sdi12_sal_10:  { propertyName: "SoilSalinity", layer: "Soil" },
  dnd_90_sdi12_sal_20:  { propertyName: "SoilSalinity", layer: "Soil" },
  dnd_90_sdi12_sal_30:  { propertyName: "SoilSalinity", layer: "Soil" },
  dnd_90_sdi12_sal_40:  { propertyName: "SoilSalinity", layer: "Soil" },
  dnd_90_sdi12_sal_50:  { propertyName: "SoilSalinity", layer: "Soil" },
  dnd_90_sdi12_sal_60:  { propertyName: "SoilSalinity", layer: "Soil" },
  dnd_90_sdi12_sal_70:  { propertyName: "SoilSalinity", layer: "Soil" },
  dnd_90_sdi12_sal_80:  { propertyName: "SoilSalinity", layer: "Soil" },
  dnd_90_sdi12_sal_90:  { propertyName: "SoilSalinity", layer: "Soil" },
  velocity:             { propertyName: "WindSpeed", unitOfMeasure: "KilometerPerHour", layer: "Atmosphere" },
  vr_winddir:           { propertyName: "WindDirection", layer: "Atmosphere" },
  rain:                 { propertyName: "Precipitation", unitOfMeasure: "Millimetre", layer: "Atmosphere" },
  nplwtemp1:            { propertyName: "LeafWetness", layer: "Vegetation" },
  nplwtemp2:            { propertyName: "LeafWetness", layer: "Vegetation" },
  wsht30_temp:          { propertyName: "AirTemperature", unitOfMeasure: "Celsius", layer: "Atmosphere" },
  dnd_90_sdi12_temp_10: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  dnd_90_sdi12_temp_20: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  dnd_90_sdi12_temp_30: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  dnd_90_sdi12_temp_40: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  dnd_90_sdi12_temp_50: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  dnd_90_sdi12_temp_60: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  dnd_90_sdi12_temp_70: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  dnd_90_sdi12_temp_80: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  dnd_90_sdi12_temp_90: { propertyName: "SoilTemperature", unitOfMeasure: "Celsius", layer: "Soil" },
  nplwlw1:              { propertyName: "LeafWetness", layer: "Soil" },
  nplwlw2:              { propertyName: "LeafWetness", layer: "Soil" },
  ext_copernicus:       { propertyName: "Evapotranspiration", unitOfMeasure: "Millimetre", layer: "Atmosphere" }
};

// We create FSM measurement data for each set of measurements in this fashion:
// 1. create a mappedMeasurement object for each measurement
// 2. create a mappedObservationCollection for each property
// 3. Group mappedObservationCollections of different layers in different arrays
// 4. Create a mapped measurement layer from each array
// 5. Create the final FSM measurement data by passing the mapped measurement layers to mapMeasurementLocationGroup 
const measurementObjects = measurementsArray.map( measurements => {
  const dateTime =  contextualizer.unixToISO8601(measurements.m_date);
  let observationCollections = {
    Atmosphere: [],
    Surface: [],
    Soil: [],
    Vegetation: []
  };
  for (const [sensorId, sensorProperties] of Object.entries(sensorConfiguration)) {
    const mappedMeasurement = contextualizer.mapMeasurement(sensorProperties.propertyName, 
      sensorId,
      dateTime, 
      measurements[sensorId]
    );
    const mappedObservationCollection = contextualizer.mapObservationCollection(sensorProperties.propertyName,
      sensorId,
      sensorProperties.unitOfMeasure,
      [mappedMeasurement]
    );
    observationCollections[sensorProperties.layer].push(mappedObservationCollection);
  }
  observationCollections = Object.fromEntries(
    Object.entries(observationCollections).map( ([layerName, layerObservationCollection]) => 
      [layerName, contextualizer.mapMeasurementLayerGroup(layerName, layerObservationCollection)]
    )
  );
  return contextualizer.mapMeasurementLocationGroup("WeatherStation", locationId, Object.values(observationCollections));
});

// We group the FSM Objects by type of data and store in an array
const FSMDataObjects = [
  { 
    typeOfData: "location",
    FSMData: locationObject 
  },
  { 
    typeOfData: "measurements",
    FSMData: measurementObjects 
  }
];

// Pass the name of tha dataset and the FSMDataObjects to the contextualizer to create the final FSM dataset
const FSMData = contextualizer.contextualizeDataset("measurements", FSMDataObjects);

// Output JSON file path
const outputJSONFilePath = "json/NP2FSM_gaiasense_output.json";

// write to JSON file
try { 
  fs.writeFileSync(outputJSONFilePath, JSON.stringify(FSMData, null, 2), 'utf8');
  console.log(`FSM data written to file ${outputJSONFilePath}`);
} catch (error) {
  console.error("Error writing file");
}

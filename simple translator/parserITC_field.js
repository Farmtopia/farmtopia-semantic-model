const fs = require("fs");
const contextualizer = require("./contextualizer")

// Read the input JSON file
const jsonFile = process.argv[2];
const jsonData = fs.readFileSync(jsonFile, 'utf-8');

// Parse the input JSON data
const jsonObject = JSON.parse(jsonData);

/**
* Parse raw field-related input data to fill required properties of FSM objects
* (key names should be same as in the input JSON file, values are the parsed values)
*/
let {
  uuid: uuid,
  identifier: nationalIdentifier,
  name: name,
  areaM2: area,
  geoJSON: geoJSON,
  fieldType:{ nameEn: fieldTypeName },
  crops: inputCropsArray
} = jsonObject;

// FSM uses vocabularies to map some taxonomies terms.
// For correlating input taxonomy values to vocabulary terms (e.g. OBO, Agrovoc)
// we use a taxonomyCorrelation object (key names are the input taxonomy values, values are 
// the official vocabulary identifiers)
const taxonomyCorrelation = {
  "Arable land": "obo:ENVO_01001177",
  "Wheat (winter)": "c_8412"
}

// Pass the corresponding parsed data as arguments to the mapCrop function
// to create crop FSM objects in JSON-LD format
const cropObjects = inputCropsArray.map(inputCrop => 
  contextualizer.mapCrop(
    inputCrop.crop.uuid, 
    inputCrop.crop.nameEn, 
    taxonomyCorrelation[inputCrop.crop.nameEn], 
    null, 
    null
  )
);

// Pass the corresponding parsed data as arguments on the mapField function
// to create farmSites FSM objects in JSON-LD format (note that the cropObjects are also passed)
// FarmSite is a wrapper for Plots. It could be equal to a cadastral entity. For compatibility 
// reasons in most cases the farmSite has one to one relationship with the plot because most 
// vendors are not using separate concepts for farmSite (parcel, field) and plot (smaller division like zone).
// Thus mapField function is creating a farmSite and nest a plot in it.
const fieldObjects = contextualizer.mapField(uuid,
  nationalIdentifier,
  name,
  area,
  geoJSON,
  fieldTypeName,
  taxonomyCorrelation[fieldTypeName],
  cropObjects
);

// We group the FSM Objects by type of data and store in an array
const FSMDataObjects = [
  { 
    typeOfData: "FarmSites",
    FSMData: fieldObjects 
  }
];

// Pass the name of tha dataset and the FSMDataObjects to the contextualizer to create the final FSM dataset
const FSMData = contextualizer.contextualizeDataset("ITC_Farms", FSMDataObjects);

// Output JSON file path
const outputJSONFilePath = "./json/ITC2FSM_field_output.json";

// write to JSON file
try { 
  fs.writeFileSync(outputJSONFilePath, JSON.stringify(FSMData, null, 2), 'utf8');
  console.log(`FSM data written to file ${outputJSONFilePath}`);
} catch (error) {
  console.error("Error writing file");
}

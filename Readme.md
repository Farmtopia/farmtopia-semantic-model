### Farmtopia Semantic Model (FSM) ###

The Farmtopia Semantic Model is an evolution of Ploutos Common Semantic Model (PCSM) (https://gitlab.com/Ploutos-project/ploutos-common-semantic-model).

The following ontologies and vocubularies are used as namespaces within the data model:

    xml: XML standard namespace.
    rdf: The base namespace for RDF, used to define properties and types of RDF resources.
    rdfs: RDF Schema provides a data-modelling vocabulary for RDF data.
    owl: Web Ontology Language, used for defining rich and complex ontologies.
    xsd: XML Schema Definition, a way to define the structure of XML data.
    skos: Simple Knowledge Organization System, used for building controlled vocabularies and classification schemes.
    dcat: Data Catalog Vocabulary, used to describe datasets in data catalogs.
    dct: Dublin Core Terms, a vocabulary of core metadata terms.
    odlr: Open Digital Rights Language, used for expressing terms and conditions over content.
    eppo: European and Mediterranean Plant Protection Organization, likely used for taxonomical data on plants.
    foaf: Friend of a Friend, used to describe people, their activities and their relations to other people and objects.
    qudt: Quantities, Units, Dimensions, and Types ontology used for describing quantitative values and units.
    sosa: Sensor, Observation, Sample, and Actuator ontology, for describing sensors and their observations.
    saref: Smart Appliances REFerence ontology, used for integrating different aspects of smart applications.
    chebi: Chemical Entities of Biological Interest, an ontology of molecular entities focused on 'small' chemical compounds.
    foodie: A domain-specific ontology for food or agriculture.
    schema: Schema.org, a collaborative, community activity with a mission to create, maintain, and promote schemas for structured data.
    agrovoc: A multilingual controlled vocabulary covering areas of interest of the Food and Agriculture Organization (FAO) of the United Nations.
    ssn-ext: An extension of the SSN ontology.
    cf: Climate and Forecast metadata conventions, used in the context of meteorological data.
    qb: RDF Data Cube Vocabulary, for expressing multi-dimensional data cubes.
    ids: International Data Spaces Information Model, used for defining and managing secure data spaces.
    time: Time Ontology in OWL, for describing the temporal properties of resources in the world.
    obo: Open Biomedical Ontologies, a collective of ontologies structured to describe different aspects of biomedicine.
    ppdo: BBCH-based Plant Phenological Description Ontology


The following choices that been made through FSM should be justified:

    1. INSPIRE compatibility - Customisations in PCSM way of dealing with spatial entities have been held in order to support the INSPIRE directive by incorporating FOODIE ontology which already implement a basic backbone for INSPIRE. Additionaly some new classes defined to facilitate the cross reference of spatial entities.
    2. QUDT introduction - In order to support more complex, while combined units of measurement, we have interchange OM ontology with QUDT and utilize it for both inputs, observations and predictions.
    3. SKOS notations - Popular vocabularies (Agrovoc, EPPO, OBI) have been used for reference of wide used terms like crop varieties.
    4. Wide SSN adoption - To enable the common modeling of observation and predictions (forecasts) we have customise some classes and properties for wider utilization. 
    5. Support time ranges - In order to support time periods, especially for statistics, we have leveraged the power of OWL TIME ontology by customizing some classes and properties to cover the required dimensions.
    6. Slices and Collections - To avoid verbocity we have implemented grouping of data which enables us to share common properties between child data points. This have implemented for both observations/predictions and operations that are the most common shared data in farmtopia ecosystem. This is a combination of Data Cube and SSN-ext ontologies.
    7. Operation classes extension - To enable more wide description of operations we have merged PCSM related classes with FOODIE's intervention.
    8. Services and Datasets - To enable the discovery and basic description of data providers and their services we have implemented International Data Spaces Information Model. Customisations have been made in order to fullfil the required workflow which include the conjuction with Data Cube Vocabulary in order to not just describe the datasets but also respectively include the actual data points (in Slices) within a unified format.
    9. Remote sensing properties - We have introduced some individuals to describe remote sensing related properties.



### FSM Example File Explanation ###

The format for the modeled data is defined as JSON-LD. By doing so we are obligated to use a context object and a graph array. Within context we are defining all namespaces that will explicitly use within the file. Also we will include all abbreviations corresponding to object and data properties defined within the namespaces. This is useful to simplify the graph array contents and make it human readable. Within the graph array we are defining our individuals by using inheritance when possible to simplify the perception of relations between them. We have to note that such inheritance is not required and all the individuals could be in the root of the graph with id pointers cross relate them. In our example file root objects within the graph are a farmer, datasets, a service catalog and organizations. 

Useful info:

    - Within datasets the inheritance is extremely useful to avoid verbocity. We are grouping in inheritance level based on one or more common properties. This could vary from dataset to dataset.
    - The service catalog is defined as a general outline of the ecosystem, by respecting the data exchange and data integration modules. It covers the description and discoverability of data resources. Any further info on how to consume the data will be held on the pointed out open api page.


Important notes:

    - The general purpose of semantic ids is to be able to follow an interconnectivity among sources. This means that the final goal for these ids is to be urls that could be resolved.


## License
This project is licensed under the EUPL License - see the [LICENSE](LICENSE) file for details.

For any questions or comments contact Markos Fragkopoulos, ma.fragkopoulos@gmail.com.
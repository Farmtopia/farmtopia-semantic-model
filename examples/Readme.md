### Overview ###

The files within the folder are pairs of original and translated data for ITC and NP web services. 

For ITC, each example is the response provided from the swagger page (https://gitlab.com/Farmtopia/digital-field-book/-/tree/main/Field%20Book%20Data%20Exchange%20REST%20API) of the Farmtopia Digital Fieldbook. The json consisted of an object per service that contains the ITC model and the corresponding FSM model. The ITC2FSM_farm.json, ITC2FSM_field.json and ITC2FSM_task.json are clean examples of ITC responses that are modeled with FSM. In order to comply with the FarmtopiaServiceCatalog model, each response data points are under a sliceContent which is under a datasetSlice. .

For NP, the example is a Gaiasense station response FSM translation.  

The whole response is an instance of the serviceResource representation. This means that within the catalog of the Farmtpopia Data Exchange service such an instance can be referred. DatasetSlices could be used to include multiple data types within a single instance, like a slice for farms, a slice for fields, a slice for tasks, while sliceContents includes multiple same type data points, like task1, task2 etc. Though multiple data types within a dataset is a feature that is not supported at the moment from ITC webservices. An empty response example can be found on FSM_response_empty_instance.json, which contains only the required shallow components of a dataset FSM representation.

At the moment, care should be taken on the final implementation on respecting the Services and Datasets Wrappers as described on the GenericFSMExample.json which stands for a full serialization of the ontology concepts on a demo dataset.

An additional example is also provided that showcase an example response slices from Data Integration service in FSM.

